# SE PHP Practical

This is a small school management system where a user can view, add and delete grades, classes and students

PHP Framework
=============

Laravel 5.2  

External Modules
================

Bootstrap  
Jquery  
Bower  
SB Admin template  

<?php

namespace App\Http\Controllers;
use Session;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Models\Classroom;
use App\Http\Models\Grade;
use App\Http\Models\Student;

class ClassController extends Controller
{
    
    /**
    * Display a listing of the resource.
    * @return Response
    */
    public function index()
    {
    	$class = new Classroom();
        $classes = $class->getAllWithGrade();
       
        $grade = new Grade();
        $grades = $grade->getAll();

        /**
        * rearrange data to group by grades 
        * @return array('grade_id1'=>array('name'=>'grade_name', 'classes'=> array(class1, class2,....)), grade_id2'=> array(),...)
        */
        $result = array();
		
		foreach ($grades as $grade) {
			$result[$grade['grade_id']]['name'] = $grade['grade_name'];
			$_tmp_class_array = [];

			foreach ($classes as $class) {
				$grade_id = $class['grade_id'];

				if($grade['grade_id'] == $class['grade_id']){
					$_tmp_class_array[] = $class;
				}
			}
			$result[$grade['grade_id']]['classes']	= $_tmp_class_array;
		}

        return view('classes.view')->with('result', $result);
    }
    
    /**
    * Show the form for creating a new resource.
    * @return Response
    */
    public function create()
    {
    	$grade = new Grade();
        $grades = $grade->getAllForForm();
        
        return view('classes.add')->with('grades', $grades);
    }

    /**
    * Load data for the ajax request 
    * @return JSON object
    */
    public function getByGrade()
    {
    	$grade_id = $_GET['grade_id'];

    	$classes = new Classroom();
    	$data  = $classes->getByGrade($grade_id);
    	
    	$output =[];
        if(count($data) <= 0){
            $output =[
                'status'=>400,
                'message'=>'List Empty'
            ];
            return json_encode($output);
        }
        $output =[
            'status'=>200,
            'message'=>'Classes found',
            'class_list'=>$data
        ];
        return json_encode($output);
    }
    
    /**
    * Store a newly created resource in storage.
    * @return Response
    */
    public function store(Request $request)
    {
    	$class = new Classroom();
    	$class->grade_id = $request->grade_id;
    	$class->name = $request->name;
    	$class->created_at = date('Y-m-d H:m:s',time());
    	$class->save();

    	Session::flash('flash_message', 'Class successfully added!');

    	$output = ['code'=>200, 'msg'=>"Successfully Created"]; 
    	return redirect('classes/add')->with('output', $output);
    }

    /**
    * Display the specified resource.
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
    }

    /**
    * Show the form for editing the specified resource.
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(Request $request, $id)
    {
    }

    /**
    * Remove the specified resource from storage.
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
    	$class = Classroom::findOrFail($id);
    	$class->delete();

    	Session::flash('flash_message', 'Class successfully deleted!');

    	$output = ['code'=>200, 'msg'=>"Successfully Created"]; 
    	return redirect('classes/')->with('output', $output);
    }
}

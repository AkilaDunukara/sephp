<?php

namespace App\Http\Controllers;
use Session;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Models\Grade;

class GradeController extends Controller
{
    public function view()
    {
        $grade = new Grade();
        $grades = $grade->getAll();
        
        return view('grades.view')->with('grades', $grades);
    }

    /**
    * Display a listing of the resource.
    * @return Response
    */
    public function index()
    {
    	$grade = new Grade();
        $grades = $grade->getAll();
        
        return view('grades.view')->with('grades', $grades);
    }
    
    /**
    * Show the form for creating a new resource.
    * @return Response
    */
    public function create()
    {
    }
    
    /**
    * Store a newly created resource in storage.
    * @return Response
    */
    public function store(Request $request)
    {

    	$grade = new Grade();
    	$grade->name = $request->name;
    	$grade->created_at = date('Y-m-d H:m:s',time());
    	$grade->save();

    	Session::flash('flash_message', 'Grade successfully added!');

    	$output = ['code'=>200, 'msg'=>"Successfully Created"]; 
    	return redirect('grades/add')->with('output', $output);
    }

    /**
    * Display the specified resource.
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
    }

    /**
    * Show the form for editing the specified resource.
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(Request $request, $id)
    {
    }

    /**
    * Remove the specified resource from storage.
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
    	$grade = Grade::findOrFail($id);
    	$grade->delete();

    	Session::flash('flash_message', 'Grade successfully deleted!');

    	$output = ['code'=>200, 'msg'=>"Successfully Created"]; 
    	return redirect('grades/')->with('output', $output);
    }
}

<?php

namespace App\Http\Controllers;
use Session;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Models\Student;
use App\Http\Models\Classroom;
use App\Http\Models\Grade;

class StudentController extends Controller
{
    
    /**
    * Display a listing of the resource.
    * @return Response
    */
    public function index()
    {
    	$student = new Student();
        $students = $student->getAllWithDetails();
        
        return view('students.view')->with('students', $students);
    }
    
    /**
    * Show the form for creating a new resource.
    * @return Response
    */
    public function create()
    {
    	$grade = new Grade();
        $grades = $grade->getAllForForm();
        
        return view('students.add')->with('grades', $grades);
    }
    
    /**
    * Store a newly created resource in storage.
    * @return Response
    */
    public function store(Request $request)
    {
    	$student = new Student();
    	$student->grade_id = $request->grade_id;
    	$student->class_id = $request->class_id;
    	$student->name = $request->name;
    	$student->age = $request->age;
    	$student->address = $request->address;
    	$student->created_at = date('Y-m-d H:m:s',time());
    	$student->save();

    	Session::flash('flash_message', 'Student successfully added!');

    	$output = ['code'=>200, 'msg'=>"Successfully Created"]; 
    	return redirect('students/add')->with('output', $output);
    }

    /**
    * Display the specified resource.
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
    }

    /**
    * Show the form for editing the specified resource.
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(Request $request,$id)
    {
    }

    /**
    * Remove the specified resource from storage.
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
    	$student = Student::findOrFail($id);
    	$student->delete();

    	Session::flash('flash_message', 'Student successfully deleted!');

    	$output = ['code'=>200, 'msg'=>"Successfully Created"]; 
    	return redirect('students/')->with('output', $output);
    }

    /**
    * Display the specified resource.
    * @param  int  $class_id
    * @return Response
    */
    public function getByClass($class_id)
    {
    	$student = new Student();
        $students = $student->getByClass($class_id);

        $class = new Classroom();
        $class_data = $class->getById($class_id);
        
        return view('students.viewbyclass')->with('students', $students)->with('classes',$class_data);
    }
}

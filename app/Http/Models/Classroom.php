<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Classroom extends Model
{
    protected $id;
    protected $grade_id;
    protected $name;
    protected $created_at;
    protected $updated_at;
    protected $table = "classes";

    public function getAll(){
    	$classes = DB::table($this->table)
    				->get();
    	return Classroom::formatData($classes);  
    }

    /**
    * Get class by id
    * @param $id
    */
    public function getById($id){
        $classes = DB::table($this->table)
                    ->where('id','=',$id)
                    ->get();

        return Classroom::formatData($classes);  
    }

    /**
    * Get classes by grade
    * @param $grade_id
    */
    public function getByGrade($grade_id){
    	$classes = DB::table($this->table)
    				->where('grade_id','=',$grade_id)
    				->get();

    	return Classroom::formatData($classes);  
    }

    /**
    * Get all classes along with grade name
    * 
    */
    public function getAllWithGrade(){
        $classes = DB::table($this->table)
                    ->join('grades',$this->table.'.grade_id','=','grades.id')
                    ->select('grades.name AS grade_name',$this->table.'.*')
                    ->get();
        return Classroom::formatData($classes);  
    }

     /**
     * Format Grade data set 
     */
    private static function formatData($classDataSet){
    	$_temp_class_data_set= [];
    	foreach($classDataSet as $class){
    		$_class =[
    			'class_id'=>$class->id,
    			'grade_id'=>$class->grade_id,
    			'class_name'=>$class->name
    		];
            $_class['grade_name'] = (isset($class->grade_name)) ? $class->grade_name : '';
    		$_temp_class_data_set[] = $_class;
    	}
    	return $_temp_class_data_set;
    }
}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Grade extends Model
{
	protected $id;
	protected $name;
	protected $created_at;
	protected $updated_at;
    protected $table = "grades";

    public function getAll(){
    	$grades = DB::table($this->table)
    				->get();
    	return Grade::formatData($grades);  
    }

    public function getAllForForm(){
    	$grades = DB::table($this->table)
    				->get();
    	return Grade::formatDataForForm($grades);  
    }

    /**
    * Rearrange $grades to build an array as grade id for index and grade name as value
    * @return array('grade_id1'=>'grade_name',.....)
    */
    private static function formatDataForForm($gradeDataSet){
    	$result = array();
        foreach ($gradeDataSet as $grade) {
        	$result[$grade->id] = $grade->name;
        }
        return $result;
    }

    /**
    * Format Grade data set 
    */
    private static function formatData($gradeDataSet){
    	$_temp_grade_data_set= [];
    	foreach($gradeDataSet as $grade){
    		$_grade =[
    			'grade_id'=>$grade->id,
    			'grade_name'=>$grade->name
    		];
    		$_temp_grade_data_set[] = $_grade;
    	}
    	return $_temp_grade_data_set;
    }
}

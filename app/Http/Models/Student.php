<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Student extends Model
{
	protected $id;
	protected $grade_id;
	protected $class_id;
	protected $name;
	protected $age;
	protected $address;
	protected $created_at;
	protected $updated_at;
    protected $table = "students";

    public function getAll(){
    	$students = DB::table($this->table)
    				->get();
    	return Student::formatData($students);  
    }

    /**
    * Get students in a detailed information, with grade and class names
    */
    public function getAllWithDetails(){
    	$students = DB::table($this->table)
    				->join('grades',$this->table.'.grade_id','=','grades.id')
	   				->join('classes',$this->table.'.class_id','=','classes.id')
	   				->select('grades.name AS grade_name','classes.name AS class_name',$this->table.'.*')
    				->get();
    	return Student::formatData($students);  
    }

    /**
    * Get students by class in a detailed information, with grade and class names
    *@param int $class_id
    */
    public function getByClass($class_id){
    	$students = DB::table($this->table)
    				->join('grades',$this->table.'.grade_id','=','grades.id')
	   				->join('classes',$this->table.'.class_id','=','classes.id')
    				->where('class_id','=',$class_id)
	   				->select('grades.name AS grade_name','classes.name AS class_name',$this->table.'.*')
    				->get();

    	return Student::formatData($students);  
    }


     /**
     * Format Grade data set 
     */
    private static function formatData($studentDataSet){
    	$_temp_student_data_set= [];
    	foreach($studentDataSet as $student){
    		$_student =[
    			'student_id'=>$student->id,
    			'grade_id'=>$student->grade_id,
    			'class_id'=>$student->class_id,
    			'student_name'=>$student->name,
    			'student_age'=>$student->age,
    			'student_address'=>$student->address
    		];
    		$_student['grade_name'] = (isset($student->grade_name)) ? $student->grade_name : '';
    		$_student['class_name'] = (isset($student->class_name)) ? $student->class_name : '';
    		$_temp_student_data_set[] = $_student;
    	}
    	return $_temp_student_data_set;
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/**
* Load Dashboard
*/
Route::get('/', function () {
    return view('dashbord/index');
});

/**
* Load Add Forms
*/
Route::get('/grades/add', function () {
    return view('grades.add');
});
Route::get('/classes/add','ClassController@create');
Route::get('/students/add','StudentController@create');

/**
* Load List Views
*/
Route::get('/grades/','GradeController@index');
Route::get('/classes/','ClassController@index');
Route::get('/students/', 'StudentController@index');
Route::get('/students/{class_id}', 'StudentController@getByClass');

/**
* Load ajax data
*/
Route::get('/classes/{grade_id}', 'ClassController@getByGrade');

/**
* save data
*/
Route::post('/grades/save','GradeController@store');
Route::post('/classes/save','ClassController@store');
Route::post('/students/save','StudentController@store');

/**
* delete data
*/
//Route::resource('classes','ClassController@destroy');
Route::delete('classes/{id}', array('as' => 'classes.destroy','uses' => 'ClassController@destroy'));
Route::delete('grades/{id}', array('as' => 'grades.destroy','uses' => 'GradeController@destroy'));
Route::delete('students/{id}', array('as' => 'students.destroy','uses' => 'StudentController@destroy'));
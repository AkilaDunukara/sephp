/**
* Main javascript functions are listed here
*/

function loadClassesByGrade(grade_id){

	$('#class_id').empty();
	$.ajax({
		method: 'GET',
		url : APP_URL+'/classes/{grade_id}',
		data: {grade_id : grade_id},
		type:"JSON",
		success: function(data){
			var data = JSON.parse(data);//PARSE SERVER DATA AS ACTUAL JSON OBJECT
				
			if(parseInt(data.status) === 200){
				dropdownTemplate(data.class_list)
			}
		}
	});
}

function dropdownTemplate(classList){
	var _html = '<label for="class_id">Class</label>';
	_html += '<select class="form-control" name="class_id" id="class_id">';

	for(var i=0; i<classList.length; i++){
		var _class = classList[i];
		_html += '<option value="'+_class.class_id+'">'+_class.class_name+'</option>';
	}

	_html += '</select>';

	$('#class_div').html(_html);
}

/**
* Load functions that requires to run on page load
*/
$( window ).load(function() {
	var grade_id = $('#grade_id').find("option:first-child").val();
	loadClassesByGrade(grade_id);
});
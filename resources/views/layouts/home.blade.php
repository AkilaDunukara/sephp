<!DOCTYPE html>
<html>
<head>

	@include('includes.head')
	<title>@yield('title')</title>
	
	<script type="text/javascript">
		var APP_URL = {!! json_encode(url('/')) !!}
	</script>

</head>
<body>
<!-- Shell -->	
	<div id="wrapper">
		@include('includes.header')
	
		<div id="page-wrapper">
			@yield('content')
		</div>
		
		@include('includes.footer')
	</div>
<!-- End Shell -->	
</body>
</html>
@extends('layouts.home')
@section('title','Lyceum International School | Add Students')
@section('content')

<div class="row">
    <div class="col-lg-10">
        <h1 class="page-header"><i class="fa fa-graduation-cap fa-fw"></i> Add Students</h1>
    </div>
    <!-- /.col-lg-10 -->
    <div class="col-lg-2">
    	<br><br>
    	<a href="{{URL::to('/students/')}}" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> View All Students</a>
    	<br><br>
    </div>
</div>
@if(Session::has('flash_message'))
<div class="alert alert-success">{{ Session::get('flash_message') }}</div> 
@endif

<div class="row">
    <div class="col-lg-12">
		{!! Form::open(array('url' => '/students/save')) !!}
		<div class="form-group">
			{!!  Form::label('name', 'Student Name') !!}
			{{ Form::text("name", '', array_merge(['class' => 'form-control'], array('id'=>'name'))) }}
		</div>

		<div class="form-group">
			{!!  Form::label('grade_id', 'Grade') !!}
			{!! Form::select('grade_id', $grades, null, ['class' => 'form-control','id' => 'grade_id' , 'onChange' => 'loadClassesByGrade(this.value)']) !!}
		</div>

		<div class="form-group" id="class_div">
			{!!  Form::label('class_id', 'Class') !!}
			{!! Form::select('class_id', array(), null, ['class' => 'form-control','id' => 'class_id' ]) !!}
		</div>

		<div class="form-group">
			{!!  Form::label('age', 'Student Age') !!}
			{{ Form::text("age", '', array_merge(['class' => 'form-control'], array('id'=>'age'))) }}
		</div>

		<div class="form-group">
			{!!  Form::label('address', 'Student Address') !!}
			{{ Form::text("address", '', array_merge(['class' => 'form-control'], array('id'=>'address'))) }}
		</div>

		<div class="form-group">
			{!!  Form::submit('Save', ['class' => 'btn btn-primary']) !!} 
		</div>

		{!! Form::close() !!}
	</div>
</div>

@endsection
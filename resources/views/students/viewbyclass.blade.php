@extends('layouts.home')
@section('title','Lyceum International School | View Students of class '.$classes[0]['class_name'])
@section('content')

<div class="row">
    <div class="col-lg-10">
        <h2 class="page-header"><i class="fa fa-graduation-cap fa-fw"></i> Class {{$classes[0]['class_name']}}</h2>
        <h4 class="sub-header">Student List</h4>
    </div>
    <!-- /.col-lg-10 -->
    <div class="col-lg-2">
    	<br><br>
    	<a href="{{URL::to('/classes/')}}" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> View All Classes</a>
    	<br><br>
    </div>
</div>

<table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th>Student Name</th>
      <th>Grade</th>
      <th>Class</th>
      <th>Age</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($students as $student)
    <tr>
      <th scope="row">{{$student['student_id']}}</th>
      <td>{{$student['student_name']}}</td>
      <td>{{$student['grade_name']}}</td>
      <td>{{$student['class_name']}}</td>
      <td>{{$student['student_age']}}</td>
      <td><a href="#" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; 
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['students.destroy', $student['student_id']],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::submit(' X ', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
      </td>
    </tr>
     @endforeach
    
  </tbody>
</table>
@endsection
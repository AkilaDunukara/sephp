@extends('layouts.home')
@section('title','Lyceum International School | View Students')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><i class="fa fa-graduation-cap fa-fw"></i> View All Students</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
@if(Session::has('flash_message'))
<div class="alert alert-success">{{ Session::get('flash_message') }}</div> 
@endif
<table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th>Student Name</th>
      <th>Grade</th>
      <th>Class</th>
      <th>Age</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($students as $student)
    <tr>
      <th scope="row">{{$student['student_id']}}</th>
      <td>{{$student['student_name']}}</td>
      <td>{{$student['grade_name']}}</td>
      <td>{{$student['class_name']}}</td>
      <td>{{$student['student_age']}}</td>
      <td><a href="#" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; 
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['students.destroy', $student['student_id']],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::submit(' X ', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
      </td>
    </tr>
     @endforeach
    
  </tbody>
</table>
@endsection
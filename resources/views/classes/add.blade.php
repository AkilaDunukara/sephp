@extends('layouts.home')
@section('title','Lyceum International School | Add Classes')
@section('content')

<div class="row">
    <div class="col-lg-10">
        <h1 class="page-header"><i class="fa fa-table fa-fw"></i> Add Classes</h1>
    </div>
    <!-- /.col-lg-10 -->
    <div class="col-lg-2">
    	<br><br>
    	<a href="{{URL::to('/classes/')}}" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> View All Classes</a>
    	<br><br>
    </div>
</div>
@if(Session::has('flash_message'))
<div class="alert alert-success">{{ Session::get('flash_message') }}</div> 
@endif

<div class="row">
    <div class="col-lg-12">
		{!! Form::open(array('url' => '/classes/save')) !!}
		<div class="form-group">
			{!!  Form::label('name', 'Class Name') !!}
			{{ Form::text("name", '', array_merge(['class' => 'form-control'], array('id'=>'name'))) }}
		</div>

		<div class="form-group">
			{!!  Form::label('name', 'Grade') !!}
			{!! Form::select('grade_id', $grades, null, ['class' => 'form-control'], array('id'=>'grade_id')) !!}
		</div>

		<div class="form-group">
			{!!  Form::submit('Save', ['class' => 'btn btn-primary']) !!} 
		</div>

		{!! Form::close() !!}
	</div>
</div>
@endsection
@extends('layouts.home')
@section('title','Lyceum International School | View Classes')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><i class="fa fa-table fa-fw"></i> View Classes</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

@if(Session::has('flash_message'))
<div class="alert alert-success">{{ Session::get('flash_message') }}</div> 
@endif

@foreach($result as $grades)
  <h3 class="sub-header">{{$grades['name']}}</h3>
  @if(count($grades['classes'])>0)
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Class Name</th>
        <th class="text-center">Students</th>
        <th class="text-center">Action</th>
      </tr>
    </thead>
    <tbody>
    	@foreach($grades['classes'] as $class)
      <tr>
        <th scope="row">{{$class['class_id']}}</th>
        <td>{{$class['class_name']}}</td>
        <td class="text-center"><a href="{{URL::to('/students/'.$class['class_id'])}}" class="btn btn-default"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
        <td class="text-center"><a href="#" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; 
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['classes.destroy', $class['class_id']],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::submit(' X ', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
        </td>
      </tr>
       @endforeach
      
    </tbody>
  </table>
  @else
  <span class="col-md-12 alert alert-warning text-center">No Classes assigned</span>
  @endif
@endforeach
@endsection
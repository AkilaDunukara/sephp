@extends('layouts.home')
@section('title','Lyceum International School | View Grades')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><i class="fa fa-cubes fa-fw"></i> View Grades</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

@if(Session::has('flash_message'))
<div class="alert alert-success">{{ Session::get('flash_message') }}</div> 
@endif

<table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th>Grade</th>
      <th class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($grades as $grade)
    <tr>
      <th scope="row">{{$grade['grade_id']}}</th>
      <td>{{$grade['grade_name']}}</td>
      <td class="text-center">
      <a href="#" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; 
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['grades.destroy', $grade['grade_id']],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::submit(' X ', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
      </td>
    </tr>
     @endforeach
    
  </tbody>
</table>
@endsection
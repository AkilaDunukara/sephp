<footer class="footer">
  <div class="container">
    <p class="text-muted text-center">Copyright &copy; LIS 2017. All rights reserved.</p>
  </div>
</footer>

<!-- jQuery -->
<script src="{{ URL::to('lib/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::to('lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ URL::to('js/sb-admin-2.js')}}"></script>

<script src="{{ URL::to('js/site.js')}}"></script>


<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{URL::to('/')}}">Lyceum International School</a>
    </div>
    <!-- /.navbar-header -->

    
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                
                <li>
                    <a href="{{URL::to('/')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                
                <li>
                    <a href="{{URL::to('/grades/add')}}"><i class="fa fa-cubes fa-fw"></i> Add Grades</a>
                </li>
                <li>
                    <a href="{{URL::to('/classes/add')}}"><i class="fa fa-table fa-fw"></i> Add Classes</a>
                </li>
                <li>
                    <a href="{{URL::to('/students/add')}}"><i class="fa fa-graduation-cap fa-fw"></i> Add Students</a>
                </li>
                
                
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
@extends('layouts.home')
@section('title','Lyceum International School | Dashbord')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Dashboard</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-cubes fa-5x"></i>
                    </div>
                    
                </div>
            </div>
            <a href="{{URL::to('/grades/')}}">
                <div class="panel-footer">
                    <span class="pull-left">View Grades</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-table fa-5x"></i>
                    </div>
                    
                </div>
            </div>
            <a href="{{URL::to('/classes/')}}">
                <div class="panel-footer">
                    <span class="pull-left">View Classes</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-graduation-cap fa-5x"></i>
                    </div>
                    
                </div>
            </div>
            <a href="{{URL::to('/students/')}}">
                <div class="panel-footer">
                    <span class="pull-left">View Students</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    
</div>
@endsection
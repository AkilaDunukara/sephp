SE - PHP - Practical Instructions
=================================

#Installation 

1. Clone the project

2. Install vendor modules using following command  
	- composer install  

3. Update .env file in following areas  
	- APP_URL  
	- DB_DATABASE  
	- DB_USERNAME  
	- DB_PASSWORD  

4. Install dependancy files run following command on git bash  
	- bower install  

5. Install database tables  
	- php artisan migrate  


#Modules

* There are 3 modules  
	1. Grades  
	2. Classes  
	3. Students  

#URLs in the system

APP_URL/public  
APP_URL/public/grades  
APP_URL/public/classes  
APP_URL/public/students  
APP_URL/public/students/{class_id}  
APP_URL/public/grades/add  
APP_URL/public/classes/add  
APP_URL/public/students/add  

#Used Technology

1. Laravel 5.2  
2. Bootstrap  
3. Jquery  
